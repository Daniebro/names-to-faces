//
//  PersonCell.swift
//  Project10
//
//  Created by Danni Brito on 12/12/19.
//  Copyright © 2019 Danni Brito. All rights reserved.
//

import UIKit

class PersonCell: UICollectionViewCell {
    
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var name: UILabel!
}
